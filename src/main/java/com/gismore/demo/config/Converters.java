package com.gismore.demo.config;

import org.aeonbits.owner.Converter;

import java.lang.reflect.Method;

class Converters {

	public static class DevModeConverter implements Converter<Boolean>{

		@Override
		public Boolean convert(Method method, String input) {
			return input.equalsIgnoreCase("dev");
		}
	}
}
