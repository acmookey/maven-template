package com.gismore.demo.config;

import com.gismore.demo.common.util.PropertiesUtils;
import org.aeonbits.owner.Config;
import org.aeonbits.owner.ConfigCache;
import org.aeonbits.owner.ConfigFactory;

import java.util.Properties;

import static com.gismore.demo.common.util.ClassPathUtils.classPath;

/**
 * Created by lei on 2017/6/23.
 */

public interface AppConfig extends Config{


	@DefaultValue("true")
	@Key("env")
	@ConverterClass(Converters.DevModeConverter.class)
	Boolean devMode();


	static AppConfig me(){
		String configKey = "appConfig";

		if(ConfigCache.get(configKey) == null){
			String configFileName = "application.properties";

			Properties properties = PropertiesUtils.loadConfigProperties(configFileName);

			AppConfig appConfig = ConfigFactory.create(AppConfig.class, properties);
			ConfigCache.add(configKey,appConfig);
		}
		return ConfigCache.get(configKey);
	}

	static void init(){
		System.setProperty("log4j.configurationFile",classPath("conf/log4j2.xml"));

		String configFileName = "application.properties";
		Properties properties = PropertiesUtils.loadConfigProperties(configFileName);
		AppConfig appConfig = ConfigFactory.create(AppConfig.class, properties);
		ConfigCache.add("appConfig",appConfig);
	}
}
