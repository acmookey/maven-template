package com.gismore.demo;

import com.gismore.demo.config.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 应用入口
 */
public class Main {
	public static void main(String[] args) {
		AppConfig.init();
		Logger log = LogManager.getLogger(Main.class);
		log.debug("log4j2 config complete!");
	}
}
