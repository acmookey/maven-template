package com.gismore.demo.common.util;

import com.google.common.base.Charsets;
import com.google.common.io.Files;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

import static com.gismore.demo.common.util.ClassPathUtils.classPath;

/**
 * Created by lei on 2017/6/23.
 */
public class PropertiesUtils {

	public static Properties loadConfigProperties(String configFileName){
		String configFilePath = classPath("conf/"+configFileName);
		Properties properties = new Properties();
		try {
			String configFileContent = Files.toString(new File(configFilePath), Charsets.UTF_8);
			properties.load(new StringReader(configFileContent));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}

	public static String loadConfigContent(String configFileName){
		String configFilePath = classPath("conf/"+configFileName);
		String configFileContent = null;
		try {
			configFileContent = Files.toString(new File(configFilePath), Charsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return configFileContent;
	}

}
