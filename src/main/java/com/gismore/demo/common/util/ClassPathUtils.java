package com.gismore.demo.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.google.common.io.Resources;

public class ClassPathUtils {

	public static String classPath(){
		return classPath("");
	}

	public static String classPath(String relativePath){
		if(relativePath == null){
			relativePath = "";
		}
		String classPath = Resources.getResource(PropertiesUtils.class,"").getFile();
		String configFileBasePath;
		Pattern jarPattern = Pattern.compile("^(file:)*(?<classPath>\\S+)/\\S+\\.jar\\S+$");
		Matcher jarMatcher = jarPattern.matcher(classPath);
		if(jarMatcher.find()){
			configFileBasePath = jarMatcher.group("classPath")+"/";
		}else{
			configFileBasePath = StringUtils.substringBefore(classPath,"classes/")+"classes/";
		}
		return configFileBasePath+relativePath;
	}
}
