package com.gismore.demo;

import com.gismore.demo.config.AppConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

/**
 * Unit test for simple Main.
 */
public class AppTest{

    @Test
    public void test(){
    	AppConfig.init();
        AppConfig appConfig = AppConfig.me();
        Logger log = LogManager.getLogger(AppTest.class);
        log.debug("log4j2 config complete!");
        Boolean devMode = appConfig.devMode();
        log.debug("devMode:" + devMode);
        Assert.assertTrue(devMode);
    }
}
