# Introduction

## 基本类库
+ [Guava: Google Core Libraries For Java](http://mvnrepository.com/artifact/com.google.guava/guava)
+ [Apache Commons Lang](http://mvnrepository.com/artifact/org.apache.commons/commons-lang3)
+ [基于注解自动生成Pojo代码 -
Project Lombok](http://mvnrepository.com/artifact/org.projectlombok/lombok)

    - Github : [https://github.com/rzwitserloot/lombok](https://github.com/rzwitserloot/lombok)
    - Lombok Features : [https://projectlombok.org/features/all](https://projectlombok.org/features/all)
+ [配置管理 - OWNER :: Java 8 Support](http://mvnrepository.com/artifact/org.aeonbits.owner/owner-java8)

    - Github : [https://github.com/lviggiano/owner](https://github.com/lviggiano/owner)
    - 中文文档 : [https://github.com/cyfonly/owner-doc](https://github.com/cyfonly/owner-doc)
+ [Log4j2 - Apache Log4j API](http://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api)
+ [Log4j2 - Apache Log4j Core](http://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-core)

    - [Log4j2 配置参考](http://logging.apache.org/log4j/2.x/manual/configuration.html)


